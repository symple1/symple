// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Various arbitrary-precision numeric types.
//!
//! All numeric types implement `From<u128>` or `From<i128>`.
//!
//! For conciseness, I refer to `u128` and `Natural` as `Into<Natural>` below.
//! The operations described do not currently apply to external types that
//! implement `Into<Natural>`, `Into<Integer>`, etc.
//!
//! `Natural`s are closed under addition, and multiplication with
//! `Into<Natural>`s, and powers of `Natural`s. Subtraction results in an
//! `Integer`. `Integer`s are closed under addition, subtraction, and
//! multiplication with all `Into<Integer>` types, and under powers of
//! `Natural`s. Division (or a power of `i128` or `Integer`) will result in a
//! `Rational`. `Rational`s are closed under addition, subtraction,
//! multiplication, and division with all `Into<Rational>` types, and powers of
//! `Into<Integer>` types.
//!
//! The `Log` or `FirstRoot<Into<Integer>>` of a `Natural` returns a
//! `RealExpression`. `Real` and `RealExpression` do not implement `Ord`, and are
//! closed under addition, multiplication, subtraction, division, and logarithms
//! with `Into<RealExpression>`s.
//!
//! COMING SOON: `Root`, `RealExpressions`, `Complex`, `ComplexExpression`, and
//! `ComplexExpressions`

use symple_core::UintVec;

mod natural;
// mod integer;
// mod rational;
// mod real;
// mod complex;
// mod surreal;

pub struct Natural(UintVec);