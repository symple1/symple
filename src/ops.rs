// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Traits for mathematical operations.

pub use core::ops::{
    Add,
    AddAssign,
    Div,
    DivAssign,
    Index,
    IndexMut,
    Mul,
    MulAssign,
    Neg,
    Rem,
    RemAssign,
    Shl,
    ShlAssign,
    Shr,
    ShrAssign,
    Sub,
    SubAssign
};

pub trait Abs {
    type Output;
    fn abs(self) -> Self::Output;
}

// pub trait DivRem<Rhs = Self> {
//     type Output;
//     fn div_rem(self, rhs: Rhs) -> Self::Output;
// }

// pub trait Inv {
//     type Output;
//     fn inv(self) -> Self::Output;
// }

// pub trait Pow<Rhs = Bigit> {
//     type Output;
//     #[must_use]
//     fn pow(self, rhs: Rhs) -> Self::Output;
// }

// pub trait FirstRoot<Rhs = Self> {
//     type Output;
//     fn first_root(self, rhs: Rhs) -> Self::Output;
// }

// pub trait Root<Rhs = Self> {
//     type Output;
//     fn root(self, rhs: Rhs) -> Self::Output;
// }

// pub trait BaseLog {
//     type Output;
//     fn lg(self) -> Self::Output;
//     fn ln(self) -> Self::Output;
// }

// pub trait Log<Rhs> {
//     type Output;
//     fn log(self, rhs: Rhs) -> Self::Output;
// }